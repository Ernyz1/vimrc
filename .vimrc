"  If you don't understand a setting in here, just type ':h setting'.

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" Make backspace behave in a sane manner.
set backspace=indent,eol,start

" Switch syntax highlighting on
syntax on

" Enable file type detection and do language-dependent indenting.
filetype plugin indent on

" Show line numbers
set number

" Tab settings
:set tabstop=4
:set shiftwidth=4
:set expandtab
:set smarttab

" Allow hidden buffers, don't limit to 1 file per window/split
set hidden

" Tab navigation (Ctrl+arrows to navigate; Alt+arrows to move tabs)
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
nnoremap <silent> <A-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <A-Right> :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>

" Vim plug
call plug#begin()
Plug 'vim-airline/vim-airline'
call plug#end()

" Vim airline
set ttimeoutlen=50 " Removes the delay which occurs when leaving insert mode

